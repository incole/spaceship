using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship_Controller : MonoBehaviour
{
    public float MoveAcceleration = 10;
    public float RotateAcceleration = 10;
    private Rigidbody _rigidbody;
    
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var vertical = Input.GetAxis("Vertical");
        var horizontal = Input.GetAxis("Horizontal");
        var hover = Input.GetAxis("Hover");

        Debug.Log(hover);
        
        _rigidbody.velocity += transform.forward * vertical * MoveAcceleration * Time.deltaTime;
        
        transform.Rotate(transform.up, horizontal * RotateAcceleration * Time.deltaTime, Space.Self);
        transform.Rotate(transform.forward, hover * RotateAcceleration * Time.deltaTime, Space.Self);
    }
}
